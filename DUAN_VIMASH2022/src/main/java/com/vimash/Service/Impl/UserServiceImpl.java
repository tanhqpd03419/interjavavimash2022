package com.vimash.Service.Impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.vimash.Dao.UserDao;
import com.vimash.Service.UserService;
import com.vimash.entity.User;

public class UserServiceImpl implements UserService{

	@Autowired
	UserDao userDao;
	
	public User createUser(String json) {
		JsonObject jsonObject = new Gson().fromJson(json, JsonObject.class);
		
		String loginId = jsonObject.get("loginId").getAsString().trim();
		String fullName= jsonObject.get("fullName").getAsString().trim();
		String email = jsonObject.get("email").getAsString().trim();
		String passWord = jsonObject.get("password").getAsString().trim();
		int phoneNumber = jsonObject.get("numberPhone").getAsInt();
		
		Date date = new Date();
		User user = new User();
		
		user.setLoginId(loginId);
		user.setFullName(fullName);
		user.setEmail(email);
		user.setPassword(passWord);
		user.setNumberPhone(phoneNumber);
		user.setCreate_date(date);
		
		
		return user;
	}
}
