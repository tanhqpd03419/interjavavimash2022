package com.vimash.Dto;

import lombok.Data;

@Data
public class OrderDto {

	private Long orderId;
	private Long userId;
	private String orderDate;
}
