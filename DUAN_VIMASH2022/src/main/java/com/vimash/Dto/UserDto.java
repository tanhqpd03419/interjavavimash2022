package com.vimash.Dto;

import lombok.Data;

@Data
public class UserDto {

	private Long userId;
	private String loginId;
	private String fullName;
	private String email;
	private int numberPhone;
}
