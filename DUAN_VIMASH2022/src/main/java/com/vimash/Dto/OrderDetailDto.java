package com.vimash.Dto;

import lombok.Data;

@Data
public class OrderDetailDto {
	private Long orderDetailId;
	private Long orderId;
	private Long productId;
	private double amount;
	private double price;
}
