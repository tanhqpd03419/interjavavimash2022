package com.vimash.Dao.Impl;



import com.vimash.Dao.AbstractBaseDao;
import com.vimash.Dao.UserDao;
import com.vimash.entity.User;

public class UserDaoImpl extends AbstractBaseDao implements UserDao{

	
	
	@Override
	public User createUser(User user) {
		
	 getEntityManager().persist(user);
	 return user;
	}
	
	@Override
	public User updateUser(User user) {
		getEntityManager().merge(user);
		return user;
	}
	
}
