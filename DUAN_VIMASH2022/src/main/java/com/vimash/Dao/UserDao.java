package com.vimash.Dao;

import com.vimash.entity.User;

public interface UserDao {

	User updateUser(User user);

	User createUser(User user);

}
